﻿using System;
namespace Upwork.TestIssue.DatabaseWrapper
{
    public class Country
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
