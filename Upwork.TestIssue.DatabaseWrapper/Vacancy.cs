﻿using System;

namespace Upwork.TestIssue.DatabaseWrapper
{
    public class Vacancy
    {
        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Salary
        {
            get;
            set;
        }

        public Location Location
        {
            get;
            set;
        } = new Location();
    }
}
