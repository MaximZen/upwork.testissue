﻿
using System;
namespace Upwork.TestIssue.DatabaseWrapper
{
    public class City
    {
       public int Id
        {
            get;
            set;
        }

        public int CountryId
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
