﻿using System;
namespace Upwork.TestIssue.DatabaseWrapper
{
    public class Location
    {
        public Country Country
        {
            get;
            set;
        } = new Country();

        public City City
        {
            get;
            set;
        } = new City();
    }
}
